/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Dell
 */
public class ViewBookAndRecommendation extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            if(request.getParameter("bookid")!=null){
                
                model.Model cModel=new model.Model();
                int bookid=Integer.parseInt((String)request.getParameter("bookid"));
                int userid=1;
                java.util.HashMap<String,String> bookdetails=cModel.getBookDetails(bookid);
                request.setAttribute("justPurchased",bookdetails.get("book_title"));
                textminer.TfIdfMain result=new textminer.TfIdfMain();
                int categoryId=Integer.parseInt(bookdetails.get("category_id"));
                String isbn=bookdetails.get("isbn13");
                
                    java.util.ArrayList<String[]> contentBasedRecommendation=result.getrecommendation(categoryId,isbn);
                
                //since book has been purchased now. Now recommend some books below
                java.util.ArrayList<String> booksAccordingToFriendsWhoBuyThisBook=cModel.getRecommendationsOnUserBasis(userid,bookid);
                java.util.ArrayList<String> booksAccordingToFriendsWhoBuyAnyBook=cModel.getRecommendationsOnUserBasis(userid);
                cModel.getRecommendationsOnContentBasis(userid);
                request.setAttribute("booksAccordingToFriendsWhoBuyThisBook",booksAccordingToFriendsWhoBuyThisBook);
                request.setAttribute("booksAccordingToFriendsWhoBuyAnyBook",booksAccordingToFriendsWhoBuyAnyBook);
                request.setAttribute("contentBasedRecommendation",contentBasedRecommendation);
                javax.servlet.RequestDispatcher dispatcher=request.getRequestDispatcher("viewbookandrecommendations.jsp");
                dispatcher.forward(request, response);
            }else{
                out.println("You need to select a id then only can View any book");
            }
        }catch(Exception e){
            e.printStackTrace();
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
