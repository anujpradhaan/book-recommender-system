/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package textminer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author manish
 */
public class Bycategorydistribution {
    Connection con;
    public void dividebycategory(String src) throws SQLException, ClassNotFoundException, IOException{
        con=Database.DatabaseManager.getConnection();
        for(int i=1;i<=5;i++)
        {
        File dir = new File(src+i);
        dir.mkdir();
        File destination;
  
        PreparedStatement ps=con.prepareStatement("select filepath from description where category_id= "+i);
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
            String filepath=rs.getString(1);
            File s=new File(src+filepath);
            destination=new File(dir+"\\"+filepath);
            Path srcpath=s.toPath();
            System.out.println("src:"+srcpath.toString()+"dest:"+destination.getPath().toString());
            Files.copy(srcpath,destination.toPath());
          
        }
        }
       
        
    }
    
    public static void main(String[] args){
        Bycategorydistribution b=new Bycategorydistribution();
        try {
            b.dividebycategory("E:\\recommender\\updated\\");
        } catch (SQLException ex) {
            Logger.getLogger(Bycategorydistribution.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Bycategorydistribution.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Bycategorydistribution.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
