/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package textminer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class DocumentParser {

    //This variable will hold all terms of each document in an array.
    private List<String[]> termsDocsArray = new ArrayList<String[]>();
    private List<String> allTerms = new ArrayList<String>(); //to hold all terms
    private List<double[]> tfidfDocsVector = new ArrayList<double[]>();
    Connection con;
    public int targetcount=0;//to know the position of target file
    private List<Double> resultofvector = new ArrayList<Double>();
    
    
    
    public DocumentParser() throws SQLException, ClassNotFoundException
    {
        con=Database.DatabaseManager.getConnection();
        PreparedStatement ps=con.prepareStatement("truncate vector");
        ps.execute();
    }
 
    /**
     */
    
    
    public void parseFiles(String filePath,String target) throws FileNotFoundException, IOException, SQLException, ClassNotFoundException {
        
      
     File[] allfiles = new File(filePath).listFiles();
        BufferedReader in = null;
        int cnt=0;
        PreparedStatement ps;
        for (File f : allfiles) {
            if (f.getName().endsWith(".txt")) {
               
               //check for target file
                String filename=f.getName().substring(2,f.getName().length()-4);
                System.out.println("filename="+filename);
                if(filename.equalsIgnoreCase(target)){
                    targetcount=cnt;
                    System.out.println("targetcount="+targetcount);
                }
                //dbentry for vector
                ps=con.prepareStatement("insert into vector values(0,'"+filename+"','0')");
                ps.execute();

                //file reading
                in = new BufferedReader(new FileReader(f));
                StringBuilder sb = new StringBuilder();
                String s = null;
                while ((s = in.readLine()) != null) {
                    sb.append(s);
                }
                String[] tokenizedTerms = sb.toString().replaceAll("[\\W&&[^\\s]]", "").split("\\W+");   //to get individual terms
                for (String term : tokenizedTerms) {
                    if (!allTerms.contains(term)) {  //avoid duplicate entry
                        allTerms.add(term);
                    }
                }
                termsDocsArray.add(tokenizedTerms);//add string arry in list for tf calculation
            }
            cnt++;
        }
        

    }

    /**
     * Method to create termVector according to its tfidf score.
     */
    public void tfIdfCalculator() {
        double tf; //term frequency
        double idf; //inverse document frequency
        double tfidf; //term requency inverse document frequency  
        int i=0;
        for (String[] docTermsArray : termsDocsArray) {
            double[] tfidfvectors = new double[allTerms.size()];
            int count = 0;
            i++;
            for (String terms : allTerms) {
                tf = new TfIdf().tfCalculator(docTermsArray, terms);
     /*           for(String a:docTermsArray){
                    System.out.println(a+"_");
                }*/
                System.out.println("term:"+terms +" Term Freq:"+ tf);
                idf = new TfIdf().idfCalculator(termsDocsArray, terms);
                System.out.println("term:"+terms +" Term IDF:"+ idf);
                
                tfidf = tf * idf;
                System.out.println("tfidf("+terms+",D"+i+")="+tfidf);
                tfidfvectors[count] = tfidf;
                count++;
            }
            for(double a:tfidfvectors){
                System.out.println("tfidfvector="+a);
            }
            tfidfDocsVector.add(tfidfvectors);  //storing document vectors;            
        }
    }

    /**
     * Method to calculate cosine similarity between all the documents.
     */
    public void getCosineSimilarity() throws SQLException {
       PreparedStatement ps;
            for (int j = 0; j < tfidfDocsVector.size(); j++) {
                /*for(double a:tfidfDocsVector.get(targetcount)){
                    System.out.println("vector="+a);
                }*/
             double answer=new CosineSimilarity().cosineSimilarity
                                       (
                                         tfidfDocsVector.get(targetcount), 
                                         tfidfDocsVector.get(j)
                                       );
                System.out.println("between " + targetcount + " and " + j + "  =  "+answer);  
                ps=con.prepareStatement("update vector set vector='"+answer+"' where sr_no="+(j+1));
                ps.execute();
                
        }
        
    }
    public ArrayList<String[]> findmatchingdocuments() throws SQLException{
            PreparedStatement ps=con.prepareStatement("select d.title,d.id,d.isbn13,d.publisher from description d,vector v where d.isbn13=v.isbn13 and v.vector!='0.0' order by v.vector desc limit 3  ");
            System.out.println("Matching documents are");
                    ArrayList<String[]> recom=new ArrayList<String[]>();
                    
            ResultSet rs=ps.executeQuery();
            while(rs.next()){
              
                recom.add(new String[]{
                    rs.getString(1),
                    rs.getString(2)
                });
                
                
                System.out.println("title="+rs.getString(1));
            }
            return  recom;
            }

   public void removestopword(String name,String destination) throws FileNotFoundException, IOException, SQLException {
          File[] allfiles = new File(name).listFiles();
        BufferedReader in = null;
         PrintWriter writer ;
          Stopwords stobj=new Stopwords();
        for (File f : allfiles) {
            ArrayList<String> lines = new ArrayList<String>();
            if (f.getName().endsWith(".txt")) {
               in = new BufferedReader(new FileReader(f));
                 
               StringBuilder sb = new StringBuilder();
                String s = null;
                while ((s = in.readLine()) != null) {
                    sb.append(s);
                }
                String[] tokenizedTerms = sb.toString().replaceAll("[0-9\\W&&[^\\s]]", " ").split("\\W+");   //to get individual terms
                      
                for(String a:tokenizedTerms){
                
                    if(stobj.isStopWord(a)){
                       System.out.println("stop word "+a);
                   }
                   else{
                        lines.add(a);
                       //fw.write(a+"\n");
                   }
                }
                 in.close();
                 String filename=destination+"r_"+f.getName();
                 writer = new PrintWriter(filename, "UTF-8") ; 
                
           
            for(String l : lines){
                 writer.print(l);
                 writer.print(" ");
                 
            }
            String isbn=f.getName().substring(0,f.getName().length()-4);
            String filepath="r_"+f.getName();
            //System.out.println("isbn="+isbn+"filepath="+filepath);   
            PreparedStatement ps= con.prepareStatement("update description set filepath='"+filepath+"' where isbn13='"+isbn+"'");
            if(!ps.execute())
               System.out.println("filepath updated");
             writer.close();
                
            }
            }
       
    }
}
