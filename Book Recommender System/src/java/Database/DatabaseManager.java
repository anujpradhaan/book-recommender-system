/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dell
 */
public class DatabaseManager {
    
    private static Connection con=null;
    
    private DatabaseManager(){
        
    }
    public static Connection getConnection() throws SQLException, ClassNotFoundException{
        
        if(con==null){
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/recommender", "root", "");
                  System.out.println("connection established");   
        }
        
        return con;
        
    }
    
    
   
}
