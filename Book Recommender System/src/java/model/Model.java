/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Dell
 */
public class Model {
    
    public java.util.HashMap<Integer,String> getAllUsersWhichAreNotFriends(int userid){//for which you want the list
        java.util.HashMap<Integer,String> userList=new java.util.HashMap<Integer,String>();
        try{
           java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement(); 
           java.sql.ResultSet rs=stmt.executeQuery("select * from user where id not in(select friendId from friendList where userid="+userid+")");
           while(rs.next()){
               userList.put(Integer.parseInt(rs.getString("id")),rs.getString(2));
           }
        }catch(Exception e){
            e.printStackTrace();
        }
        return userList;
    }
    
    
    public void addFriend(int userid,int friendid){
        String query="insert into friendlist(userid,friendid,trustfactor)values("+userid+","+friendid+",5)";
        try{
            java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
            stmt.execute(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public java.util.ArrayList<String>  getRecommendationsOnUserBasis(int userid,int bookid){
        java.util.ArrayList<String> bookList=new java.util.ArrayList<String>();
        try{
            if((isFriendExists(userid)||isPurchseDetailsExists(userid))&& doFriendsHavePurchasedAny(userid,bookid)){
                /*
                 If user have no friend then it is definetly a cold start.
                 */
                System.out.println("publisher basis with bookid");
                String query="select books.* from books,publisher where books.book_publisher_id=publisher.publisherId order by publisherrating desc";
                java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
                java.sql.ResultSet rs=stmt.executeQuery(query);
                while(rs.next()){
                    bookList.add(rs.getString("bookid")+","+rs.getString("book_title"));
                }
            }else{
                ///get all the book purchsed by your friends
               // String query="select book.* from books b,(select book_id,user_id from purchaseDetails where book_id!="+bookid+" and user_id in(select friendId from friendlist where userid="+userid+")) p where b.bookid=p.book_id order by rating desc;";
               
                //get all the books by your friends who have purchased this book
                String query="select * from books b,(select book_id,user_id from purchaseDetails where book_id!="+bookid+" and user_id in(select friendId from friendlist f,purchasedetails pd where userid="+userid+" and f.friendId=pd.user_id and pd.book_id="+bookid+")) p where b.bookid=p.book_id order by rating desc";
                java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
                java.sql.ResultSet rs=stmt.executeQuery(query);
                while(rs.next()){
                    bookList.add(rs.getString("bookid")+","+rs.getString("book_title"));
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return bookList;
    }
    
    ///get all the book purchsed by your friends
    public java.util.ArrayList<String>  getRecommendationsOnUserBasis(int userid){
        java.util.ArrayList<String> bookList=new java.util.ArrayList<String>();
        try{
            if((isFriendExists(userid)||isPurchseDetailsExists(userid))&& doFriendsHavePurchasedAny(userid)){
                /*
                 If user have no friend then it is definetly a cold start.
                 */
                System.out.println("publisher basis without bookid");
                String query="select * from books,publisher where books.book_publisher_id=publisher.publisherId order by publisherrating desc";
                java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
                java.sql.ResultSet rs=stmt.executeQuery(query);
                while(rs.next()){
                    bookList.add(rs.getString("bookid")+","+rs.getString("book_title"));
                }
            }else{
                String query="select * from books b,(select book_id,user_id from purchaseDetails where user_id in(select friendId from friendlist where userid="+userid+")) p where b.bookid=p.book_id order by rating desc";
                java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
                java.sql.ResultSet rs=stmt.executeQuery(query);
                while(rs.next()){
                    bookList.add(rs.getString("bookid")+","+rs.getString("book_title"));
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return bookList;
    }
    
    public void changePublisherRanking(int bookid,int userid){
        
    }
    
    private boolean isAuthoritativeUser(int userid){
        boolean flag=false;
        try{
            String query="select * from user where id="+userid;
            java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);        
        }catch(Exception e){
            e.printStackTrace();
        }
        return flag;
    }
    public java.util.ArrayList<String>  getRecommendationsOnContentBasis(int userid){
        return null;
    }

    public java.util.HashMap<String,String> getBookDetails(int bookid){
        java.util.HashMap<String,String> bookdetail=new java.util.HashMap<String,String>();
        String query="select * from books where bookid="+bookid;
        try{
            java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                bookdetail.put("isbn13",rs.getString("isbn13"));
                bookdetail.put("category_id",rs.getString("category_id"));
                bookdetail.put("book_title",rs.getString("book_title"));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return bookdetail;
    }
    
    private boolean doFriendsHavePurchasedAny(int userid,int bookid){//return false if exists
        boolean flag=true;
        
        try{
            String query="select book_id,user_id from purchaseDetails where book_id!="+bookid+" and user_id in(select friendId from friendlist f,purchasedetails pd where userid="+userid+" and f.friendId=pd.user_id and pd.book_id="+bookid+")";
            java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                flag=false;
                return flag;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return flag;
    }
    
    private boolean doFriendsHavePurchasedAny(int userid){//return false if exists
        boolean flag=true;
        
        try{
            String query="select book_id,user_id from purchaseDetails where user_id in(select friendId from friendlist where userid="+userid+")";
            java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                flag=false;
                return flag;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return flag;
    }
    
    public void purchaseBook(int bookId,int userId){
        String query="insert into purchasedetails(book_id,user_id)values("+bookId+","+userId+")";
        try{
            java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
            stmt.execute(query);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public java.util.ArrayList<String> getAllBooks(int userid){
        java.util.ArrayList<String> bookList=new java.util.ArrayList<String>();
        try{
            if(isFriendExists(userid)||isPurchseDetailsExists(userid)){
                /*
                 If user have no friend then it is definetly a cold start.
                 */
                String query="select books.* from books,publisher where books.book_publisher_id=publisher.publisherId order by publisherrating desc";
                java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
                java.sql.ResultSet rs=stmt.executeQuery(query);
                while(rs.next()){
                    bookList.add(rs.getString("bookid")+","+rs.getString("book_title"));
                }
            }else{
                String query="select books.* from books order by rating desc";
                java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
                java.sql.ResultSet rs=stmt.executeQuery(query);
                while(rs.next()){
                    bookList.add(rs.getString("bookid")+","+rs.getString("book_title"));
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return bookList;
    }
    
    
    private boolean isPurchseDetailsExists(int userid){//if no purchse exists the return true
        boolean purchaseExists=true;
        try{
            String query="select user_id from purchasedetails where user_id="+userid;
            java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                purchaseExists=false;
                return purchaseExists;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return purchaseExists;
    }
    private boolean isFriendExists(int userid){//if no friend exist then return true
        boolean friendExists=true;
        try{
            String query="select friendid from friendlist where userid="+userid;
            java.sql.Statement stmt=SingleDBConnection.getConnectionObject().createStatement();
            java.sql.ResultSet rs=stmt.executeQuery(query);
            while(rs.next()){
                friendExists=false;
                return friendExists;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return friendExists;
    }
}
