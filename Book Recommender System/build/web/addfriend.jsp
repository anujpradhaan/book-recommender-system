<%-- 
    Document   : addfriend
    Created on : Apr 24, 2014, 3:35:37 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Select a option:</h1>
        <table>
            <tr>
                <th>
                    Add Friend
                </th>
                <th>
                    Buy Book
                </th>
                <th>
                    View Recommended Books
                </th>
            </tr>
            <tr>
                <td>
                    <a href="AddFriend"><button>Add Friend</button></a>
                </td>
                <td>
                    <a href="BuyBook"><button>Buy Books</button></a>
                </td>
                <td>
                    <a href="ViewRecommendations"><button>View Books Recommended to you</button></a>
                </td>
            </tr>
        </table>
        <h1>Select a Friend:</h1>
        <table>
            <tr>
                <th>
                    Friend Name:
                </th>
                <th>
                    Friend Name:
                </th>
            </tr>
            <%
java.util.HashMap<Integer,String> userWhichAreNotFriend=(java.util.HashMap<Integer,String>)request.getAttribute("userWhichAreNotFriend");            
java.util.Iterator iter=userWhichAreNotFriend.keySet().iterator();
while(iter.hasNext()){
    Integer key=(Integer)iter.next();
    String val=(String)userWhichAreNotFriend.get(key);
%>
            <tr>
                <td>
                    <%out.println(val);%>
                </td>
                <td>
                    <a href="<%out.println("AddFriend?id="+key);%>"><button>Add</button></a>
                </td>
            </tr>
<%
    
            
}

%>            
        </table>
    </body>
</html>
