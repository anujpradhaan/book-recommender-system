<%-- 
    Document   : index
    Created on : Apr 24, 2014, 3:29:19 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Select a option:</h1>
        <table>
            <tr>
                <th>
                    Add Friend
                </th>
                <th>
                    Buy Book
                </th>
                <th>
                    View Recommended Books
                </th>
            </tr>
            <tr>
                <td>
                    <a href="AddFriend"><button>Add Friend</button></a>
                </td>
                <td>
                    <a href="BuyBook"><button>Buy Books</button></a>
                </td>
                <td>
                    <a href="ViewRecommendations"><button>View Books Recommended to you</button></a>
                </td>
            </tr>
        </table>
    </body>
</html>
