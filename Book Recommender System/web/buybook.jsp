<%-- 
    Document   : buybook
    Created on : Apr 24, 2014, 3:41:51 PM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Buy Books here:</h1>
        <table>
            <%
java.util.ArrayList<String> bookList=(java.util.ArrayList<String>)request.getAttribute("bookslist");
java.util.Iterator<String> itr=bookList.iterator();
while(itr.hasNext()){
    String temp[]=itr.next().split(",");
%>
            <tr>
                <td><%out.println(temp[1]);%></td>
                <td><a href="<%out.println("PurchaseBook?bookid="+temp[0]);%>" target="_new"><button>Purchase</button></a></td>
                <td><a href="<%out.println("ViewBookAndRecommendation?bookid="+temp[0]);%>" target="_new"><button>View</button></a></td>
            </tr>
            <%
}            
%>
        </table>
    </body>
</html>
