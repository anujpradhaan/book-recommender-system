<%-- 
    Document   : purchase
    Created on : Apr 25, 2014, 1:10:35 AM
    Author     : Dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Purchase More Books:</h1>
        <table>
            <tr>
                <td>You have just purchased: </td>
                <td><%out.println( (String)request.getAttribute("justPurchased"));%></td>
            </tr>
        </table>
            <h1>Recommended Books</h1>    
                <h2>Books Similar to this book :</h2>
                <table>
                              <%
java.util.ArrayList<String[]> contentbookList=(java.util.ArrayList<String[]>)request.getAttribute("contentBasedRecommendation");
java.util.Iterator<String[]> contentitr=contentbookList.iterator();
while(contentitr.hasNext()){
    String temp[]=contentitr.next();
%>
            <tr>
                <td><%out.println(temp[0]);%></td>
                <td><a href="<%out.println("ViewBookAndRecommendation?bookid="+temp[1]);%>"><button>View</button></a></td>
            </tr>
            <%
}            
%>
                </table>
                <h2>Friends Who Bought This book also bought these:</h2>
                <table>
                              <%
java.util.ArrayList<String> bookList=(java.util.ArrayList<String>)request.getAttribute("booksAccordingToFriendsWhoBuyThisBook");
java.util.Iterator<String> itr=bookList.iterator();
while(itr.hasNext()){
    String temp[]=itr.next().split(",");
%>
            <tr>
                <td><%out.println(temp[1]);%></td>
                <td><a href="<%out.println("ViewBookAndRecommendation?bookid="+temp[0]);%>"><button>View</button></a></td>
            </tr>
            <%
}            
%>
                </table>
                
                <h2>Your Friends have already bought these:</h2>
                <table>
                         <%
bookList=(java.util.ArrayList<String>)request.getAttribute("booksAccordingToFriendsWhoBuyAnyBook");
itr=bookList.iterator();
while(itr.hasNext()){
    String temp[]=itr.next().split(",");
%>
            <tr>
                <td><%out.println(temp[1]);%></td>
                <td><a href="<%out.println("ViewBookAndRecommendation?bookid="+temp[0]);%>"><button>View</button></a></td>
            </tr>
            <%
}            
%>
                </table>
    </body>
</html>
